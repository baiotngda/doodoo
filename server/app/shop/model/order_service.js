const order = doodoo.bookshelf.Model.extend({
    tableName: "shop_order",
    hasTimestamps: true
});
const service = require("./service");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "shop_order_service",
    hasTimestamps: true,
    order: function() {
        return this.belongsTo(order, "order_id");
    },
    service: function() {
        return this.belongsTo(service, "service_id");
    }
});
