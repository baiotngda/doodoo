const base = require("./../../base");

module.exports = class extends base {
    async _initialize() {
        await super.isWxaAuth();
        await super.isShopAuth();
    }

    async index() {
        const shopId = this.state.shop.id;
        const category = await this.model("productcategory")
            .query(qb => {
                qb.where("shop_id", shopId);
                qb.where("status", 1);
            })
            .fetchAll();

        this.success(this.getTree(category));
    }
};
