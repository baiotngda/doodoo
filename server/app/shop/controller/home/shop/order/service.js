const base = require("./../../base");

module.exports = class extends base {
    async _initialize() {
        await super.isCustomAuth();
        await super.isAppAuth();
        await super.isShopAuth();
    }

    /**
     *
     * @api {get} /shop/home/shop/order/service/index 售后理由列表
     * @apiDescription 售后理由列表
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     * @apiHeader {String} WxaToken 小程序授权token.
     *
     * @apiParam {Number} type        类型 0：售后 1：退款
     *
     * @apiSampleRequest /shop/home/shop/order/service/index
     *
     */
    async index() {
        const shopId = this.state.shop.id;
        const { page = 1, type = "" } = this.query;
        const res = await this.model("service")
            .query(qb => {
                qb.where("shop_id", shopId);
                if (this.isSet(type)) {
                    qb.where("type", type);
                }
                qb.orderBy("id", "desc");
            })
            .fetchPage({ pageSize: 20, page: page });
        this.success(res);
    }

    /**
     *
     * @api {post} /shop/home/shop/order/service/add  新增/修改售后理由
     * @apiDescription 新增/修改售后理由
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     * @apiHeader {String} WxaToken 小程序授权token.
     *
     * @apiParam {Number} id          理由id
     * @apiParam {String} name        理由名称
     * @apiParam {Number} type        类型 0：售后 1：退款
     * @apiParam {Number} status      状态
     *
     * @apiSampleRequest /shop/home/shop/order/service/add
     *
     */

    async add() {
        const shopId = this.state.shop.id;
        const { id, name, type, status = 1 } = this.post;
        if (name) {
            const service = await this.model("service")
                .query(qb => {
                    qb.where("shop_id", shopId);
                    qb.where("name", name);
                    qb.where("type", type);
                })
                .fetch();
            if (service && id !== service.id) {
                this.fail("该理由已存在");
                return;
            }
        }

        const res = await this.model("service")
            .forge(
                Object.assign(
                    {
                        id,
                        name,
                        type,
                        status
                    },
                    {
                        shop_id: shopId
                    }
                )
            )
            .save();
        this.success(res);
    }

    /**
     *
     * @api {get} /shop/home/shop/order/service/info 售后理由详情
     * @apiDescription 售后理由详情
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id  售后理由id
     *
     * @apiSampleRequest /shop/home/shop/order/service/info
     *
     */
    async info() {
        const { id } = this.query;
        const shopId = this.state.shop.id;
        const service = await this.model("service")
            .query(qb => {
                qb.where("id", id);
                qb.where("shop_id", shopId);
            })
            .fetch();
        this.success(service);
    }

    /**
     *
     * @api {get} /shop/home/shop/order/service/update 修改售后理由状态
     * @apiDescription 修改售后理由状态
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id      优惠券id
     * @apiParam {Number} status  状态（1:开启，0：关闭）
     *
     * @apiSampleRequest /shop/home/shop/order/service/update
     *
     */
    async update() {
        const { id, status } = this.query;
        const service = await this.model("service")
            .forge({
                id,
                status
            })
            .save();
        this.success(service);
    }

    /**
     *
     * @api {get} /shop/home/shop/order/service/del 删除售后理由
     * @apiDescription 删除售后理由
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id      优惠券id
     *
     * @apiSampleRequest /shop/home/shop/order/service/del
     *
     */
    async del() {
        const { id } = this.query;
        const shopId = this.state.shop.id;
        const service = await this.model("service")
            .query(qb => {
                qb.where("id", id);
            })
            .destroy();
        this.success(service);
    }
};
