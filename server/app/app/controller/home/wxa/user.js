const base = require("./../base");
const User = require("./../../../../../lib/wxa/user.class");

module.exports = class extends base {
    async _initialize() {
        await super.isWxaAuth();
    }

    async _before() {
        this.user = new User(
            process.env.OPEN_APPID,
            process.env.OPEN_APPSECRET
        );
        try {
            this.state.wxa = await this.checkWxaAuthorizerAccessToken(
                this.state.wxa
            );
        } catch (err) {
            this.status = 500;
            throw err;
        }
    }

    /**
     *
     * @api {get} /home/wxa/user/tester 获取绑定的运营者
     * @apiDescription 获取绑定的运营者
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} WxaToken 小程序token.
     *
     * @apiSuccess {Array} success 运营者列表
     *
     * @apiSampleRequest /home/wxa/user/tester
     *
     */
    async tester() {
        const tester = await this.model("wxa_tester")
            .query({ where: { wxa_id: this.state.wxa.id } })
            .fetchAll();
        this.success(tester);
    }

    /**
     *
     * @api {get} /home/wxa/user/bindTester 绑定运营者
     * @apiDescription 绑定运营者
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} WxaToken 小程序token.
     * @apiHeader {String} Token 用户登录授权token.
     *
     * @apiParam {String} wechat_id 微信号
     *
     * @apiSuccess {JSON} success 结果
     *
     * @apiSampleRequest /home/wxa/user/bindTester
     *
     */
    async bindTester() {
        const { wechatid } = this.query;
        if (!wechatid) {
            this.fail("请输入微信号");
            return;
        }

        const wxaTester = await this.model("wxa_tester")
            .query({
                where: {
                    wxa_id: this.state.wxa.id,
                    wechatid: wechatid
                }
            })
            .fetch();
        if (!wxaTester) {
            const tester = await this.user.bind_tester(
                this.state.wxa.authorizer_access_token,
                wechatid
            );
            if (tester.errmsg === "ok" || tester.errcode === 85004) {
                await this.model("wxa_tester")
                    .forge({
                        wxa_id: this.state.wxa.id,
                        wechatid: wechatid
                    })
                    .save();
                this.success(tester);
            } else {
                this.fail(this.wxaErr(tester));
            }
        } else {
            this.fail("此微信号已是测试用户");
        }
    }

    /**
     *
     * @api {get} /home/wxa/user/unbindTester 解绑运营者
     * @apiDescription 解绑运营者
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} WxaToken 小程序token.
     * @apiHeader {String} Token 用户登录授权token.
     *
     * @apiParam {String} wechat_id 微信号
     *
     * @apiSuccess {JSON} success 结果
     *
     * @apiSampleRequest /home/wxa/user/unbindTester
     *
     */
    async unbindTester() {
        const { wechatid } = this.query;
        if (!wechatid) {
            this.fail("请输入微信号");
            return;
        }

        const wxaTester = await this.model("wxa_tester")
            .query({
                where: {
                    wxa_id: this.state.wxa.id,
                    wechatid: wechatid
                }
            })
            .fetch();
        if (wxaTester) {
            const tester = await this.user.unbind_tester(
                this.state.wxa.authorizer_access_token,
                wechatid
            );
            if (tester.errmsg === "ok") {
                await this.model("wxa_tester")
                    .query({
                        where: {
                            id: wxaTester.id,
                            wechatid: wechatid
                        }
                    })
                    .destroy();

                this.success(tester);
            } else {
                this.fail(this.wxaErr(tester));
            }
        } else {
            this.fail("此微信号不是测试用户");
        }
    }
};
