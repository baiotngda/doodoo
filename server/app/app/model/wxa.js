const wxaAudit = doodoo.bookshelf.Model.extend({
    tableName: "wxa_udit",
    hasTimestamps: true
});

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "wxa",
    hasTimestamps: true,
    audit: function() {
        return this.hasOne(wxaAudit, "wxa_id");
    }
});
