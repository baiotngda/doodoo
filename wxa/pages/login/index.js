//index.js
//获取应用实例
const app = getApp()

Page({
    data: {
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
        color: wx.getExtConfigSync().color,
        wxa: {}
    },
    onLoad: function() {
        wx.doodoo.fetch('/app/api/base/getWxaInfo').then(res => {
            if (res && res.data.errmsg == 'ok'){
                this.setData({
                    wxa: res.data.data
                });
            }
        })
    },
    getUserInfo: function(e) {
        if (e.detail.errMsg === 'getUserInfo:ok') {
            wx.setStorageSync('userInfo', e.detail.userInfo);
            wx.navigateBack();
            wx.doodoo.login(() => {
                let length = wx.doodoo.noAuthorizationRequestOptions.length;
                for (let i = 0; i < length; i++) {
                    wx.doodoo.request(
                        wx.doodoo.noAuthorizationRequestOptions.pop()
                    );
                }
            });
        } else {
            wx.redirectTo({
                url: '/pages/login/index',
                fail: res => {
                    console.log(res);
                }
            })
        }
    }
})