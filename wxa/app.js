//app.js
require('/utils/doodoo.js');

App({
    onLaunch: function() {
        if (wx.canIUse('getUpdateManager')) {
            const updateManager = wx.getUpdateManager()
            updateManager.onCheckForUpdate(function (res) {
                // 请求完新版本信息的回调
                // console.log(res.hasUpdate)
            })
            updateManager.onUpdateReady(function () {
                wx.showModal({
                    title: '更新提示',
                    content: '新版本已经准备好，是否重启应用？',
                    success: function (res) {
                        if (res.confirm) {
                            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                            updateManager.applyUpdate()
                        }
                    }
                })

            })
            updateManager.onUpdateFailed(function () {
                // 新的版本下载失败
                wx.showModal({
                    title: '更新提示',
                    content: '新版本下载失败',
                    showCancel: false
                })
            })
        } else {
            wx.showModal({
                title: '更新提示',
                content: '当前微信版本过低，无法使用自动更新功能，请升级到最新微信版本后重试。',
                showCancel: false
            })
        }
    },
    globalData: {
        userInfo: null
    },
    getUserInfo: function(cb) {
        var that = this;
        if (this.globalData.userInfo) {
            typeof cb == "function" && cb(this.globalData.userInfo);
        } else {
            //调用登录接口
            wx.login({
                success: function() {
                    wx.getUserInfo({
                        success: function(res) {
                            that.globalData.userInfo = res.userInfo;
                            wx.setStorageSync('userInfo', res.userInfo)
                            typeof cb == "function" && cb(that.globalData.userInfo);
                        },
                        fail: function(res) {
                            // 授权登录页面
                            wx.navigateTo({
                                url: '/pages/login/index'
                            });
                        }
                    });
                }
            });
        }
    }
})